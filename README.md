README
The project url opens the login page, where the user can either log in with an existing account or create a new account. Once logged in, the user is taken to the profile page, where the user can edit their profile information including name, email, phone number, graduation year, and active user status. The side navigation bar redirects to the four other pages: Courses, Topics, Find Help, and About. The Courses page is where the user can search for courses by CRN, course number, or course name and add or delete courses in their list of enrolled courses. The Topics page allows the user to choose the topics in which they can tutor. Topics are searchable by class and show up in the user's "My Topics" list. The Find Help page is where users seeking help can search for available tutors for their topics of interest. The About page redirects to Prosimo's company website.

The app is live and deployed @ www.prosimotutor.com 

We had some errors with database connection, which drops every couple of hours. If the page is unresponsive
email me at merdenbe@nd.edu and I can restart the server.

To run the webservice:
	cd webservice
	python3.6 main.py

Requirements (pip install):
	python3.6
	mysql-connector
	requests
	cherrpy
	JSON   

Testing:
	To test our client, we clicked through all the pages rigorously checking
	the devloper window to ensure there were no failed requests or features 
	that did not work. 
