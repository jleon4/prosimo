import random
import requests
import json
import mysql.connector


NAME_URL 	= 'https://uinames.com/api/'	# base name url
N 			= 50 							# number of names (1-500)
MAX_LEN		= 24							# max length of names
REGION		= 'United+States'				# regions

GRAD_YEARS 	= [2020, 2019, 2021, 2022]

def insert_user(firstName, lastName, email, gradYear, password, phone, status):
	mydb = mysql.connector.connect(
	  host="localhost",
	  user="merdenbe",
	  passwd="Rower435",
	  database="merdenbe"
	)

	mycursor = mydb.cursor()

	sql = "INSERT INTO Users (firstName, lastName, email, phone, gradYear, password, status) VALUES (%s, %s, %s, %s, %s, %s, %s)"
	val = (firstName, lastName, email, phone, gradYear, password, status)
	mycursor.execute(sql, val)

	mydb.commit()

	print(mycursor.rowcount, "record inserted.")



if __name__ == '__main__':
	# Generate url
	url = NAME_URL + '?amount={}&maxlen={}&region={}&ext'.format(N, MAX_LEN, REGION)
	
	# Request
	r = requests.get(url)
	resp = json.loads(r.content)

	for user in resp:
		status = random.randint(0,1)
		print(user['name'].encode("ascii"), user['surname'].encode("ascii"), user['email'].encode("ascii"), random.choice(GRAD_YEARS), user['password'].encode("ascii"), user["phone"].encode("ascii"))
		insert_user(user['name'].encode("ascii"), user['surname'].encode("ascii"), user['email'].encode("ascii"), random.choice(GRAD_YEARS), user['password'].encode("ascii"), user["phone"].encode("ascii"), status)

	insert_user('Chris', 'Foley', 'cfoley@nd.edu', 2020, 'goirish', '(314) 775 3250', 1)
	insert_user('Tim', 'Weninger', 'tweninger@nd.edu', 2013, 'ILoveComputers', '(574) 631 6770', 1)
