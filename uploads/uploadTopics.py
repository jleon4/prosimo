#!/usr/bin/env python

import csv
import mysql.connector

def insert_topic(crn, topic_num, title, desc):
	mydb = mysql.connector.connect(
		host="localhost",
		user="merdenbe",
		passwd="Rower435",
		database="merdenbe"
	)

	mycursor = mydb.cursor()

	sql = "INSERT INTO Topics (crn, topic_num, topic_title, topic_desc) VALUES (%s, %s, %s, %s)"
	val = (crn, topic_num, title, desc)
	mycursor.execute(sql, val)

	mydb.commit()

	print(val)
	print(mycursor.rowcount, "record inserted")	

if __name__ == '__main__':
	with open('./CSV_FILES/topics.csv') as file:
		csv_reader = csv.reader(file, delimiter=',')

		first = True
		for row in csv_reader:
			if first:
				first = False
				continue

			crn   		= row[0].rstrip()
			topic_num 	= row[1].rstrip()
			title 		= row[2].rstrip()
			desc  		= row[3].rstrip()


			crn.replace('"', '')
			topic_num.replace('"', '')
			title.replace('"', '')
			desc.replace('"', '')

			
			if desc[-1] is not '.':
				desc = desc + '.'

			title = title.rstrip()
			desc = desc.rstrip()
	
			insert_topic(crn, topic_num, title, desc)
