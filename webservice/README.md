Webservice README.md

Our server runs locally on port 8080.
A customer can use our server by utilizing serval different HTTP methods at
different URLS. These are outlined below

--------------------------------------------------------------------------------
Users Controller

PATH:          /users/:userId
METHOD:        GET
FUNCTION:      GET_USERID
Description:   Gets all user information in a dictionary by id

PATH:          /users/
METHOD:        POST
FUNCTION:      POST
Description:   Add a new user given the information in a dictionary format

PATH:          /users/:userId
METHOD:        PUT
FUNCTION:      PUT_USERID
Description:   Edit an existing users information with all information sent in
               dictionary format

PATH:          /users/:userId
METHOD:        DELETE
FUNCTION:      DELETE_USERID
Description:   Deletes a user from the service based on id

--------------------------------------------------------------------------------
Courses Controller

PATH:          /courses/GET_ALL
METHOD:        GET
FUNCTION:      GET_ALL
Description:   Returns a list of dictionaries of all courses in the database

PATH:         /courses/:userId
METHOD:       GET
FUNCTION:     GET_USER_COURSES
Description:  Returns list of course dictionaries that contain course meta data
              that a given userId holds

PATH:         /courses/:userId
METHOD:       POST
FUNCTION:     POST_COURSE
Description:  Adds a course to a user's current courses

PATH:         /courses/:userId'
METHOD:       PUT
FUNCTION:     DELETE_USER_COURSE
Description:  Deletes a course from a users current courses
              We chose to utilize a PUT method over a DELETE method because a
              body is necessary to send a CRN.
--------------------------------------------------------------------------------
Tutors Controller

PATH:         /tutors/:userId
METHOD:       GET
FUNTION:      GET_USERID
Description:  Gets all topics taught by the user specified by userId

PATH:         /tutors/:userId
METHOD:       PUT
FUNTION:      PUT_USERID
Description:  Deletes a topic from a user's taught topics

PATH:         /tutors/:userId
METHOD:       POST
FUNTION:      POST_USERID
Description:  Adds a topic to a user's taught topics
              We chose to utilize a PUT method over a DELETE method because a
              body is necessary to send a topic ID.
--------------------------------------------------------------------------------
Topics Controller

PATH:         /topics/:crn
METHOD:       GET
FUNCTION:     GET_CRN
Description:  Gets all the topics taught by a tutor by id
--------------------------------------------------------------------------------
Authenticate Controller

PATH:         /users/:userId
METHOD:       POST
FUNCTION:     POST_AUTHENTICATE
Description:  Returns if an email/password combination is valid

--------------------------------------------------------------------------------
Status Controller

PATH:         /status/
METHOD:       PUT
FUNCTION:     PUT_INDEX
Description:  Changes a user's availability

--------------------------------------------------------------------------------
