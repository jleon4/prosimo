import cherrypy
import json
from _prosimo_database import _prosimo_database

class SearchController(object):

    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb

    # Gets all topics taught by the user specified by userId
    def GET_HELP(self, topic_id):
        output = {"result" : "success"}
        topic_id = int(topic_id)
        data = self.mydb.find_tutor(topic_id)
        output["data"] = data 
        return json.dumps(output)
