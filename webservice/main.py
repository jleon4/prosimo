import cherrypy

from users import UsersController
from courses import CoursesController
from tutors import TutorsController
from topics import TopicsController
from authenticate import AuthenticateController
from status import StatusController
from search import SearchController

from _prosimo_database import _prosimo_database

class OptionsController:
    def OPTIONS(self, *args, **kargs):
        return ''

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Headers"] = "*"
    cherrypy.response.headers["Content-Type"] = "application/json"


def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    db = _prosimo_database()

    usersController         = UsersController(mydb = db)
    coursesController       = CoursesController(mydb = db)
    tutorsController        = TutorsController(mydb = db)
    topicsController        = TopicsController(mydb = db)
    authenticateController  = AuthenticateController(mydb = db)
    statusController        = StatusController(mydb = db)
    searchController        = SearchController(mydb = db)
    optionsController       = OptionsController()

    # Users Controller Connections
    dispatcher.connect('get_userId', '/users/:userId', controller=usersController, action='GET_USERID', conditions=dict(method=['GET']))
    dispatcher.connect('post_user', '/users/', controller=usersController, action='POST', conditions=dict(method=['POST']))
    dispatcher.connect('put_user', '/users/:userId', controller=usersController, action='PUT_USERID', conditions=dict(method=['PUT']))
    dispatcher.connect('delete_user', '/users/:userId', controller=usersController, action='DELETE_USERID', conditions=dict(method=['DELETE']))

    # Courses Controller Connections
    dispatcher.connect('get_all', '/courses/', controller=coursesController, action='GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('get_user_courses', '/courses/:userId', controller=coursesController, action='GET_USER_COURSES', conditions=dict(method=['GET']))
    dispatcher.connect('post_course', '/courses/:userId', controller=coursesController, action='POST_COURSE', conditions=dict(method=['POST']))
    dispatcher.connect('delete_user_course', '/courses/:userId', controller=coursesController, action='DELETE_USER_COURSE', conditions=dict(method=['PUT']))

    # Tutors Controller Connections
    dispatcher.connect('get_user_tutors', '/tutors/:userId', controller=tutorsController, action='GET_USERID', conditions=dict(method=['GET']))
    dispatcher.connect('change_user_tutors', '/tutors/:userId', controller=tutorsController, action='PUT_USERID', conditions=dict(method=['PUT']))
    dispatcher.connect('add_user_tutor', '/tutors/:userId', controller=tutorsController, action='POST_USERID', conditions=dict(method=['POST']))

    # Topics Controller Connections
    dispatcher.connect('topics', '/topics/:crn', controller=topicsController, action='GET_CRN', conditions=dict(method=['GET']))

    # Authenticate Controller Connections
    dispatcher.connect('authenticate', '/authenticate/', controller=authenticateController, action='POST_AUTHENTICATE', conditions=dict(method=['POST']))

    # Status Controller Connections
    dispatcher.connect('check_status', '/status/', controller=statusController, action='PUT_INDEX', conditions=dict(method=['PUT']))

    # Search Controller Conncetions
    dispatcher.connect('search', '/search/:topic_id', controller=searchController, action='GET_HELP', conditions=dict(method=['GET']))

	# OPTIONS Connections
    dispatcher.connect('user_options', '/users/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('userId_options', '/users/:userId', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('courses_options', '/courses/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('coursesId_options', '/courses/:userId', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('tutorsId_options', '/tutors/:userId', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('topisCrn_options', '/topics/:crn', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('authenticate_options', '/authenticate/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('status_options', '/status/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('search_options', '/search/:topic_id', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))





	# Configuration
    conf = {
        'global' : {
            'server.socket_host' : '206.189.202.2',
            'server.socket_port' : 8080
        },
        '/' : {
            'request.dispatch' : dispatcher,
            'tools.CORS.on' : True,
        },
    }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
