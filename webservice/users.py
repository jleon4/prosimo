import cherrypy
import json
from _prosimo_database import _prosimo_database

class UsersController(object):
    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb
    # Gets all user information in a dictionary by id
    def GET_USERID(self, userId):
        output = {'result' : 'success'}

        try:
            userId = int(userId)
            user = self.mydb.get_user(userId)
            if user is not None:
                output['user'] = dict()

                output['user']['userId']    = user['userId']
                output['user']['firstName'] = user['firstName']
                output['user']['lastName']  = user['lastName']
                output['user']['email']     = user['email']
                output['user']['phone']     = user['phone']
                output['user']['gradYear']  = user['gradYear']
                output['user']['password']  = user['password']
                output['user']['status']    = user['status']
            else:
                raise Exception('user not found')

        except Exception as ex:
                output['result']    = 'error'
                output['message']   = str(ex)

        return json.dumps(output)
    # Add a new user given the information in a dictionary format
    def POST(self):
        output = {'result' : 'success'}

        try:
            data = json.loads(cherrypy.request.body.read())

            firstName   = str(data['firstName'])
            lastName    = str(data['lastName'])
            email       = str(data['email'])
            phone       = str(data['phone'])
            gradYear    = int(data['gradYear'])
            password    = hash(str(data['password']))
            status      = int(data['status'])

            userId = self.mydb.add_user(firstName, lastName, email, phone, gradYear, password, status)
            output['userId'] = userId
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)
        return json.dumps(output)

    # Edit an existing users information with all information sent in
    # dictionary format
    def PUT_USERID(self, userId):
        output = {'result' : 'success'}
        try:
            userId = int(userId)

            data = json.loads(cherrypy.request.body.read())

            firstName   = str(data['firstName'])
            lastName    = str(data['lastName'])
            email       = str(data['email'])
            phone       = str(data['phone'])
            gradYear    = int(data['gradYear'])
            status      = int(data['status'])

            self.mydb.edit_user(userId, firstName, lastName, email, phone, gradYear, status)
        except Exception as e:
            output['result']    = 'failure'
            output['message']   = str(e)

        return json.dumps(output)

    # Deletes a user from the service based on id
    def DELETE_USERID(self, userId):
        output = {'result': 'success'}
        try:
            userId = int(userId)
            self.mydb.delete_user(userId)
        except Exception as e:
            output['result']    = 'failure'
            output['message']   = str(e)

        return json.dumps(output)
