from _prosimo_database import _prosimo_database
import unittest
import requests
import json
import requests

SITE_URL = 'http://206.189.202.2:8080'
COURSES_URL = SITE_URL + '/courses/'
TUTORS_URL = SITE_URL + '/tutors/'
STATUS_URL = SITE_URL + '/status/'
TOPICS_URL = SITE_URL + '/topics/'
AUTHENTICATE_URL = SITE_URL + '/authenticate/'
USERS_URL = SITE_URL + '/users/'

class TestWS(unittest.TestCase):
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    """ unit tests for prosimo server """

    #################################
    #           Test USERS        #
    #################################
    # Test GET_USERID
    def test_USERS_GET_USERID(self):
        # Get User
        userId = '22'
        m = dict()
        r = requests.get(USERS_URL + userId)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['user']['firstName'], "Frank")
        self.assertEqual(resp['user']['email'], "frankscott@example.com")

    # Test POST
    def test_USERS_POST(self):
        # Add New User
        data = { "firstName" : "Cynthia", "lastName": "Hamilton", "email" : "cynthia84@example.com", "phone": "(209) 665 5106", "gradYear" : 2020, "password" : "Hamilton84!(", "status": 1 }
        r = requests.post(USERS_URL, data = json.dumps(data))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        userId = str(resp["userId"])
        self.assertEqual(resp['result'], 'success')

        # Check if the new user is there
        r = requests.get(USERS_URL + userId)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['user']['firstName'], "Cynthia")

    # Test PUT_USERID
    def test_USERS_PUT_USERID(self):
        userId = '138'
        # Change user
        data = { "firstName" : "Cynthia", "lastName": "TEST", "email" : "cynthia84@example.com", "phone": "(209) 665 5106", "gradYear" : 2020, "password" : "Hamilton84!(", "status": 1 }
        r = requests.put(USERS_URL + userId, data = json.dumps(data))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # Check if Changed
        r = requests.get(USERS_URL + userId)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['user']['lastName'], "TEST")

        # Change back
        data = { "firstName" : "Cynthia", "lastName": "test", "email" : "cynthia84@example.com", "phone": "(209) 665 5106", "gradYear" : 2020, "password" : "Hamilton84!(", "status": 1 }
        r = requests.put(USERS_URL + userId, data = json.dumps(data))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

    # Test DELETE_USERID
    def test_USERS_DELETE_USERID(self):
        # Adds new user
        data = { "firstName" : "Cynthia", "lastName": "Hamilton", "email" : "cynthia84@example.com", "phone": "(209) 665 5106", "gradYear" : 2020, "password" : "Hamilton84!(", "status": 1 }
        r = requests.post(USERS_URL, data = json.dumps(data))
        resp = json.loads(r.content.decode())
        userId = str(resp["userId"])

        # Deletes that new user
        m = dict()
        r = requests.delete(USERS_URL + userId, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # Check if deleted
        r = requests.get(USERS_URL + userId)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'error')

    #################################
    #           Test COURSES        #
    #################################
    # Test GET_ALL
    def test_COURSES_GET_ALL(self):
        m = dict()
        test = {'this should hold the actual values'}
        r = requests.get(COURSES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['courses'][0]['crn'], 10012)
        self.assertEqual(resp['courses'][1]['title'], "Research and Dissertation")

    # Test GET_USER_COURSES
    def test_COURSES_GET_USER_COURSES(self):
        userId = '22'
        r = requests.get(COURSES_URL + userId)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['courses'][0]["crn"], 10189)
        self.assertEqual(resp['courses'][1]["title"], "CSE Service Projects")

    # Test POST_COURSE
    def test_COURSES_POST_COURSE(self):
        userId = '22'
        m = {'crn' : '20119'}
        r = requests.post(COURSES_URL + userId, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r1 = requests.get(COURSES_URL + userId)
        self.assertTrue(self.is_json(r1.content.decode()))
        resp = json.loads(r1.content.decode())
        crns = set()
        for c in resp['courses']:
            crns.add(c['crn'])
        self.assertTrue(20119 in crns)

    # Test DELETE_USER_COURSE
    def test_COURSES_DELETE_USER_COURSE(self):
        userId = '22'
        m = {'crn' : '20119'}

        # Adds it
        r = requests.post(COURSES_URL + userId, data = json.dumps(m))

        # Delete
        r2 = requests.put(COURSES_URL + userId, data = json.dumps(m))

        # Gets to check
        r3 = requests.get(COURSES_URL + userId)
        # Ensures that the crn isn't in
        crns = set()
        for c in json.loads(r3.content.decode())['courses']:
            crns.add(str(c['crn']))

        self.assertTrue(m['crn'] not in crns)


    #################################
    #           Test TUTORS        #
    #################################
    # Test GET_USERID
    def test_TUTORS_GET_USERID(self):
        userId = '22'
        m = dict()
        r = requests.get(TUTORS_URL + userId)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['topics'][0]["crn"], 26102)
        self.assertEqual(resp['topics'][1]["topic_title"], "Files (System calls)")

    # Test POST
    def test_TUTORS_POST(self):
        userId = '22'
        data = {'topic_id' : 45}

        # ensure that topic isn't already there
        r = requests.get(TUTORS_URL + userId)
        resp = json.loads(r.content.decode())
        tops = set()
        for t in resp['topics']:
            tops.add(t['topic_id'])

        # place it in
        r = requests.post(TUTORS_URL + userId, data=json.dumps(data))
        self.assertTrue(self.is_json(r.content.decode()))
        self.assertEqual(resp['result'], 'success')

        # make sure it's in there now
        r = requests.get(TUTORS_URL + userId)
        resp = json.loads(r.content.decode())
        tops = set()
        for t in resp['topics']:
            tops.add(t['topic_id'])

        self.assertTrue(data['topic_id'] in tops)

        # delete it
        r = requests.put(TUTORS_URL + userId, data = json.dumps(data))

    # Test PUT_USERID
    def test_TUTORS_PUT_USERID(self):
        userId = '22'
        data = {'topic_id' : '2'}
        #delete topic from userId 22
        r = requests.put(TUTORS_URL + userId, data = json.dumps(data))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # place it in
        r = requests.post(TUTORS_URL + userId, data=json.dumps(data))
        self.assertTrue(self.is_json(r.content.decode()))
        self.assertEqual(resp['result'], 'success')

        # make sure it's in there now
        r = requests.get(TUTORS_URL + userId)
        resp = json.loads(r.content.decode())
        tops = set()
        for t in resp['topics']:
            tops.add(t['topic_id'])

        self.assertTrue(data['topic_id'] not in tops)

        # delete it
        r = requests.put(TUTORS_URL + userId, data = json.dumps(data))






    #################################
    #           Test TOPIS        #
    #################################
    # Test GET_CRN
    def test_TOPICS_GET_CRN(self):
        crn = '10189'
        m = dict()
        r = requests.get(TOPICS_URL + crn)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['topics'][0]['topic_title'], "Propositional Logic")

    #################################
    #           Test Authenticate   #
    #################################
    # Test POST_AUTHENTICATE
    def test_AUTHENTICATE_POST_AUTHENTICATE_PASS(self):
        m = {"email" : "merdenbe@nd.edu", "password" : "password"}
        r = requests.post(AUTHENTICATE_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'success')

    def test_AUTHENTICATE_POST_AUTHENTICATE_FAIL(self):
        m = {"email" : "FAILMEEEE", "password" : "FAILMEEEE"}
        r = requests.post(AUTHENTICATE_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        self.assertEqual(resp['result'], 'error')

    #################################
    #           Test STATUS   #
    #################################
    # Test PUT_INDEX
    def test_STATUS_PUT_INDEX(self):
        # Test turning on
        data = {"userId" : 22, "status" : 1}
        r = requests.put(STATUS_URL, data = json.dumps(data))

        self.assertTrue(self.is_json(r.content.decode()))

        r = requests.get(USERS_URL + str(data["userId"]))
        resp = json.loads(r.content.decode())['user']

        self.assertEqual(resp['status'], data['status'])

        # Test turning off
        data["status"] = 0
        r = requests.put(STATUS_URL, data = json.dumps(data))

        self.assertTrue(self.is_json(r.content.decode()))

        r = requests.get(USERS_URL + str(data["userId"]))
        resp = json.loads(r.content.decode())['user']

        self.assertEqual(resp['status'], data['status'])


if __name__ == "__main__":
    unittest.main()
