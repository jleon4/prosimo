import cherrypy
import json
from _prosimo_database import _prosimo_database

class TopicsController(object):

    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb

    # Gets all the topics taught by a tutor by id
    def GET_CRN(self, crn):
        output = {"result" : "success"}
        try:
            crn = int(crn)

            topics = self.mydb.get_course_topics(crn)
            output["topics"] = list()
            for tup in topics:
                course = dict()
                course['topic_id']      = tup[0]
                course['topic_title']   = tup[1]
                course['topic_desc']    = tup[2]
                output['topics'].append(course)


        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
