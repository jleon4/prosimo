import cherrypy
import json
from _prosimo_database import _prosimo_database

class AuthenticateController(object):

    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb

    # Returns if an email/password combination is valid
    def POST_AUTHENTICATE(self):
        output = {'result':'success'}
        data = json.loads(cherrypy.request.body.read())
        try:
            email = str(data['email'])
            password = hash(str(data['password']))
            result = self.mydb.authenticate(email, password)
            if not result:
                raise Exception('incorrect email/password combination')
            output['userId'] = result
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
