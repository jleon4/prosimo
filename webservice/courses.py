import cherrypy
import json
from _prosimo_database import _prosimo_database

class CoursesController(object):

    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb

    # Returns a list of dictionaries of all courses in the database
    def GET_ALL(self):
        output = {'result' : 'success'}
        try:
            courses = self.mydb.get_all_courses()

            if not courses:
                raise Exception('SQL had no result')

            output['courses'] = []
            for course in courses:
                output['courses'].append({
                    'crn'          : course[0],
                    'courseNumber'  : course[1],
                    'title'         : course[2],
                })

        except Exception as e:
            output['result']    = 'failure'
            output['message']   = str(e)

        return json.dumps(output)

    # Returns list of course dictionaries that contain course meta data
    # that a given userId holds
    def GET_USER_COURSES(self, userId):
        output = {'result' : 'success'}
        try:
            userId = int(userId)

            courses = self.mydb.get_user_courses(userId)

            output['courses'] = []

            for course in courses:
                output['courses'].append({
                    'crn'          : course[0],
                    'courseNumber'  : course[1],
                    'title'         : course[2],
                })

        except Exception as e:
            output['result']    = 'error'
            output['message']   = str(e)

        return json.dumps(output)

    # Adds a course to a user's current courses
    def POST_COURSE(self, userId):
        output = {'result' : 'success'}
        try:
            data    = json.loads(cherrypy.request.body.read().decode())
            crn     = int(data["crn"])
            userId  = int(userId)

            self.mydb.add_course(userId, crn)
        except Exception as e:
            output['result']    = 'error'
            output['message']   = str(e)

        return json.dumps(output)

    # Deletes a course from a users current courses
    def DELETE_USER_COURSE(self, userId):
        output = {'result' : 'success'}
        try:
            data    = json.loads(cherrypy.request.body.read())
            crn     = int(data["crn"])
            userId  = int(userId)
            self.mydb.del_user_course(userId, crn)
        except Exception as e:
            output['result']    = 'error'
            output['message']   = str(e)

        return json.dumps(output)
