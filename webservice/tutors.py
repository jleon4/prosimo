import cherrypy
import json
from _prosimo_database import _prosimo_database

class TutorsController(object):

    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb

    # Gets all topics taught by the user specified by userId
    def GET_USERID(self, userId):
        output = {"result" : "success"}

        try:
            userId = int(userId)
            topics = self.mydb.get_user_topics(userId)

            if not topics:
                raise Exception('userId not in database')

            output["topics"] = list()
            for tup in topics:
                course = dict()
                course['crn']           = tup[0]
                course['topic_title']   = tup[1]
                course['rating']        = tup[2]
                course['num_sessions']  = tup[3]
                course['topic_id']      = tup[4]

                output["topics"].append(course)

        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)

    # Deletes a topic from a user's taught topics
    def PUT_USERID(self, userId):
        output = {"result" : "success"}

        try:
            data = json.loads(cherrypy.request.body.read())
            userId  = int(userId)
            tid  = int(data['topic_id'])

            self.mydb.del_topic(userId, tid)
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)

    # Adds a topic to a user's taught topics
    def POST_USERID(self, userId):
        output = {"result" : "success"}

        try:
            data = json.loads(cherrypy.request.body.read())
            userId = int(userId)
            tid = int(data['topic_id'])
            self.mydb.add_topic(userId, tid)
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)
