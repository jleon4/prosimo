import mysql.connector
import json

# Prosimo Database Class

class _prosimo_database:
    def __init__(self):
        # Creates connection to database
        self.mydb = mysql.connector.connect(
            host="206.189.202.2",
            user="root",
            passwd="some_pass",
            database="Prosimo"
        )


    def reconnect(self):
        if not self.mydb.is_connected():
            self.mydb.close()
            self.mydb = mysql.connector.connect(
                host="206.189.202.2",
                user="root",
                passwd="some_pass",
                database="Prosimo"
            )

    '''
        authenticate: checks email password combination
        args    - email, password
        returns - SQL data
    '''
    def authenticate(self, email, password):
        self.reconnect()
        mycursor    = self.mydb.cursor()
        mycursor.execute("SELECT * FROM Users WHERE email = '{}' and password = '{}'".format(email, password))
        result      = mycursor.fetchall()
        mycursor.close()

        if not result:
            return None

        return result

    '''
        get_all_courses: returns list of dictionaries of all courses
        args    - None
        returns - SQL data
    '''
    def get_all_courses(self):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT * FROM Courses"
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()
        return result


    '''
        add_course: adds a course to the Enrolled table for a user
        args    - userId, crn
    '''
    def add_course(self, userId, crn):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "INSERT INTO Enrolled (crn, userId) VALUES ({}, {})".format(crn, userId)
        mycursor.execute(sql)
        self.mydb.commit()

    '''
        del_course: deletes a course from the Enrolled table for a user
        args    - userId, crn
        returns - SQL data
    '''
    def del_user_course(self, userId, crn):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "DELETE FROM Enrolled where crn = {} and userId = {}".format(crn, userId)
        mycursor.execute(sql)
        self.mydb.commit()
        mycursor.close()
        # TODO: figure out how to check if SQL ran successfully

    '''
        get_user_courses: returns all courses for a given user as a list of dictionaries
        args    - userId
        returns - SQL data
    '''
    def get_user_courses(self, userId):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT Courses.crn, Courses.courseNumber, Courses.title FROM Courses, Enrolled WHERE Enrolled.userId = {} and Courses.crn = Enrolled.crn".format(userId)
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()
        return result

    '''
        get_user_topics: returns all topics a given user teaches as a list of dictionaries
        args    - userId
        returns - SQL data
    '''
    def get_user_topics(self, userId):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT Topics.crn, Topics.topic_title, Tutors.rating, Tutors.num_sessions, Topics.topic_id from Tutors, Topics where user_id = {} and Topics.topic_id = Tutors.topic_id".format(userId)
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()
        return result

    '''
        add_topic: adds a topic to Tutors table for a user
        args    - userId, topicId
        returns - SQL data
    '''
    def add_topic(self, userId, topic_id):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "INSERT INTO Tutors (user_id, topic_id, rating, num_sessions) VALUES (%s, %s, %s, %s)"
        val = (userId, topic_id, 0, 0) # default rating and num_sessions
        result = mycursor.execute(sql, val)
        self.mydb.commit()
        mycursor.close()



    '''
        del_topic: deletes a topic in Tutors table for a user
        args    - userId, topicId
    '''
    def del_topic(self, userId, topic_id):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "DELETE FROM Tutors where topic_id = {} and user_id = {}".format(topic_id, userId)
        mycursor.execute(sql)
        self.mydb.commit()
        mycursor.close()

    '''
        get_course_topics: gets all topics for a given course as a list of dictionaries
        args    - crn
        returns - SQL data
    '''
    def get_course_topics(self, crn):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT topic_id, topic_title, topic_desc FROM Topics WHERE crn = {}".format(crn)
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()
        return result

    '''
        find_tutor: finds an available tutor that teaches specific topic and is available
        args    - topic_id
        returns - SQL data
    '''
    def find_tutor(self, topic_id):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT Users.userId FROM Tutors, Users WHERE Tutors.topic_id = {} and Users.status = 1 and Users.userId = Tutors.user_id".format(topic_id)
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()
        return result

    '''
        switch_status_on: changes status to on
        args    - userId
    '''
    def switch_status_on(self, userId):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "UPDATE Users SET status = {} where userId = {}".format("1", userId)
        mycursor.execute(sql)
        self.mydb.commit()
        mycursor.close()

    '''
        switch_status_off: changes status to on
        args    - userId
    '''
    def switch_status_off(self, userId):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "UPDATE Users SET status = {} where userId = {}".format("0", userId)
        mycursor.execute(sql)
        self.mydb.commit()
        mycursor.close()

    '''
        log_session: saves the topic_id, users involved, and timestamp of each session
        args    - topic_id, pupil_id, tutor_id
    '''
    def log_session(self, topic_id, pupil_id, tutor_id):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "INSERT INTO Sessions (pupil_id, tutor_id, topic_id) VALUES ({}, {}, {})".format(topic_id, pupil_id, tutor_id)
        mycursor.execute(sql)
        self.mydb.commit()
        mycursor.close()

    '''
        get_user: gets user by Id and returns all information
        args - userId
        returns - dictionary of information on user
    '''
    def get_user(self, userId):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT * FROM Users WHERE userId = {}".format(userId)
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()

        if not result:
            return None

        u = result[0]
        user = {
                    'userId'    : userId,
                    'firstName' : u[0],
                    'lastName'  : u[1],
                    'email'     : u[3],
                    'phone'     : u[5],
                    'gradYear'  : u[4],
                    'password'  : u[6],
                    'status'    : u[7]
                }
        return user

    '''
        get_user_by_name: returns information on user by name
        args - firstName, lastName
        returns - dictionary of information
    '''
    def get_user_by_name(self, firstName, lastName):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "SELECT * FROM Users WHERE firstName = '{}' and lastName = '{}'".format(firstName, lastName)
        mycursor.execute(sql)
        result = mycursor.fetchall()
        mycursor.close()

        if not result:
            return None

        u = result[0]
        user = {
                        'userId'    : u[2],
                        'firstName' : u[0],
                        'lastName'  : u[1],
                        'email'     : u[3],
                        'phone'     : u[5],
                        'gradYear'  : u[4],
                        'password'  : u[6],
                        'status'    : u[7]
                    }
        return user

    '''
        add_user: adds user to the database
        args    - all user information
    '''
    def add_user(self, firstName, lastName, email, phone, gradYear, password, status):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "INSERT INTO Users (firstName, lastName, email, phone, gradYear, password, status) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        val = (firstName, lastName, email, phone, gradYear, password, status)
        mycursor.execute(sql, val)
        self.mydb.commit()
        mycursor.close()

        return mycursor.lastrowid

    '''
        edit_user: edits an existing users profile information
        args    - all user information
    '''
    def edit_user(self, userId, firstName, lastName, email, phone, gradYear, status):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "UPDATE Users SET firstName = %s, lastName = %s, email = %s, phone = %s, gradYear = %s, status = %s WHERE userId = %s"
        val = (firstName, lastName, email, phone, gradYear, status, userId)
        mycursor.execute(sql, val)
        self.mydb.commit()
        mycursor.close()

    '''
        delete_user: deletes a user from the database
        args    - userId
    '''
    def delete_user(self, uid):
        self.reconnect()
        mycursor = self.mydb.cursor()
        sql = "DELETE FROM Users WHERE userId = {}".format(uid)
        mycursor.execute(sql)
        self.mydb.commit()
        mycursor.close()
