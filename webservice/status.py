import cherrypy
import json
from _prosimo_database import _prosimo_database

class StatusController(object):

    def __init__(self, mydb=None):
        if mydb is None:
            self.mydb = _prosimo_database()
        else:
            self.mydb = mydb

    # Changes a user's availability
    def PUT_INDEX(self):
        output = {"result" : "success"}

        try:
            data = json.loads(cherrypy.request.body.read())
            userId = int(data['userId'])

            if int(data["status"]) == 1:
                self.mydb.switch_status_on(userId)
            elif int(data["status"]) == 0:
                self.mydb.switch_status_off(userId)
        except Exception as ex:
            output['result']    = 'error'
            output['message']   = str(ex)

        return json.dumps(output)
